<?php
// $Id: flot-views-style.tpl.php,v 1.1.2.1 2009/09/21 17:00:41 yhahn Exp $

/**
 * @file flot-views-summary-style.tpl.php
 * Template to display a flot summary view.
 *
 * - $element : An array representation of the flot DOM element.
 * - $data: A flotData object.
 * - $options: A flotStyle object.
 */
?>

<div class="views-flot">
  <?php
    static $n;
    if(isset($addselectionfilter) && $addselectionfilter){
      $element['class'] = (isset($element['class'])) ? $element['class'] . ' flot-with-zoom' : 'flot-with-zoom';
      $element['id'] = 'flot-view-zoomable-' . $n++;
      $options->selection['mode'] = 'x';
    }
    
    $vars = array('element' => $element, 'data' => $data, 'options' => $options);
    print theme_flot_graph($vars);

    if(isset($addselectionfilter) && $addselectionfilter && (!isset($options->series->pie->show) || $options->series->pie->show == FALSE)){
      $options->series->points->show = FALSE;
      $options->series->lines->lineWidth = 1;
      $options->series->shadowSize = 0;
      $options->grid->hoverable = FALSE;
      $options->selection['mode'] = 'x';
      $options->legend->show = FALSE;
      foreach ($zoomdata as &$data) {
        $data->points->show = FALSE;
      }
      $element = array(
        'id' => $element['id'] . '-zoom',
        'style' => "width:600px;height:100px",
      );
      $vars = array('element' => $element, 'data' => $zoomdata,'options' => $options);
      print theme_flot_graph($vars);
   }
   if (isset($options->legend->containerid)) {
    print '<div id="' . $options->legend->containerid . '" class="legend legend-bottom"></div>';
   }
  ?>
</div>
